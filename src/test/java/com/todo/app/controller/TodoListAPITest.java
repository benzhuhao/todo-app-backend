package com.todo.app.controller;


import com.todo.app.model.TodoItem;
import com.todo.app.service.TodoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith( MockitoJUnitRunner.class )
public class TodoListAPITest {

    @InjectMocks
    private TodoListAPI todoListAPI;

    @Mock
    private TodoService todoService;

    private final TodoItem todoItem = new TodoItem();

    @Before
    public void setup() {
        todoItem.setId( 10000L );
        todoItem.setDescription( "this is test todo" );
        todoItem.setUsername( "test" );
        todoItem.setlastUpdate( "2019-08-11T12:00:00" );
        todoItem.setDone( false );
    }

    @Test
    public void testCreateTodoItem() {

        final ResponseEntity<TodoItem> responseEntity = todoListAPI.createTodoItem( new TodoItem() );

        assertEquals( HttpStatus.CREATED, responseEntity.getStatusCode() );
    }

    @Test
    public void testUpdateTodoItem() {

        final ResponseEntity<TodoItem> responseEntity = todoListAPI.updateTodoItem( new TodoItem() );

        assertEquals( HttpStatus.OK, responseEntity.getStatusCode() );
    }

    @Test
    public void testGetTodoItem() {
        when( todoService.getTodoItem( anyString(), anyLong() ) )
                .thenReturn( todoItem );

        final ResponseEntity<TodoItem> responseEntity = todoListAPI.getTodoItem( "test", 10000L );

        assertEquals( HttpStatus.OK, responseEntity.getStatusCode() );
        assertEquals( todoItem.getDescription(), responseEntity.getBody().getDescription() );
    }

    @Test
    public void testGetTodoList() {
        final List<TodoItem> todoList = new ArrayList<>();
        todoList.add( todoItem );

        when( todoService.getTodoList( anyString() ) ).thenReturn( todoList );

        final ResponseEntity<List<TodoItem>> responseEntity = todoListAPI.getTodoList( "test" );
        assertEquals( HttpStatus.OK, responseEntity.getStatusCode() );
        assertEquals( todoItem.getDescription(), responseEntity.getBody().get(0).getDescription() );
    }

    @Test
    public void testDeleteTodoItem() {

        final ResponseEntity<Void> responseEntity = todoListAPI.deleteTodoItem( 10000L );

        assertEquals( HttpStatus.NO_CONTENT, responseEntity.getStatusCode() );
    }
}
