package com.todo.app.controller;

import com.todo.app.config.JWT.JwtTokenUtil;
import com.todo.app.exception.AuthenticationException;
import com.todo.app.model.JwtTokenRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith( MockitoJUnitRunner.class )
public class JwtAuthenticationAPITest {

    @InjectMocks
    private JwtAuthenticationAPI jwtAuthenticationAPI;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private JwtTokenUtil jwtTokenUtil;

    @Mock
    private UserDetailsService jwtUserDetailsService;

    @Mock
    private HttpServletRequest request;

    private final JwtTokenRequest jwtTokenRequest = new JwtTokenRequest(  );

    @Before
    public void setup() {
        jwtTokenRequest.setUsername( "test" );
        jwtTokenRequest.setPassword( "test" );
    }

    @Test
    public void testCreateAuthenticationToken() {

        final ResponseEntity< ? > responseEntity = jwtAuthenticationAPI.createAuthenticationToken( jwtTokenRequest );

        assertEquals( HttpStatus.OK, responseEntity.getStatusCode() );
    }

    @Test
    public void testRefreshAndGetAuthenticationTokenSuccess() {

        when( request.getHeader( any() )).thenReturn( "someToken" );
        when( jwtTokenUtil.getUsernameFromToken( anyString() ) ).thenReturn( "test" );
        when( jwtTokenUtil.canTokenBeRefreshed( anyString() ) ).thenReturn( true );

        ResponseEntity<?> responseEntity = jwtAuthenticationAPI.refreshAndGetAuthenticationToken( request );

        assertEquals( HttpStatus.OK, responseEntity.getStatusCode() );
    }

    @Test(expected = AuthenticationException.class )
    public void testRefreshAndGetAuthenticationTokenFail() {

        when( request.getHeader( any() )).thenReturn( "someToken" );
        when( jwtTokenUtil.getUsernameFromToken( anyString() ) ).thenReturn( "test" );
        when( jwtTokenUtil.canTokenBeRefreshed( anyString() ) ).thenReturn( false );

        ResponseEntity<?> responseEntity = jwtAuthenticationAPI.refreshAndGetAuthenticationToken( request );

        assertEquals( HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode() );
    }

}
