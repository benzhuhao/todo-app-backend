package com.todo.app.exception;

public class TodoItemNotFountException extends RuntimeException {
    public TodoItemNotFountException( String msg ) {
        super( msg );
    }
}
