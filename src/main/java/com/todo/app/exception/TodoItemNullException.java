package com.todo.app.exception;

public class TodoItemNullException extends RuntimeException {

    public TodoItemNullException( String message ) {
        super( message );
    }
}
