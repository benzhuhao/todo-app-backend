package com.todo.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class ExceptionHandlers {

    @ExceptionHandler( { AuthenticationException.class } )
    public ResponseEntity< String > handleAuthenticationException( AuthenticationException e ) {
        return ResponseEntity
                .status( HttpStatus.UNAUTHORIZED )
                .body( e.getMessage() );
    }

    @ExceptionHandler( {TodoItemNullException.class} )
    public ResponseEntity< String > handleTodoItemNullException( TodoItemNullException e ) {
        return ResponseEntity
                .status( HttpStatus.BAD_REQUEST )
                .body( e.getMessage() );
    }

    @ExceptionHandler( {TodoItemNotFountException.class} )
    public ResponseEntity< String > handleTodoItemNotFountException(TodoItemNotFountException e) {
        return ResponseEntity
                .status( HttpStatus.NOT_FOUND )
                .body( e.getMessage() );
    }
}
