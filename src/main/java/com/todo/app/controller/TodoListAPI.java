package com.todo.app.controller;

import com.todo.app.model.TodoItem;
import com.todo.app.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping( value = "/todo" )
@CrossOrigin
public class TodoListAPI {

    @Autowired
    private TodoService todoService;

    @PostMapping(
            value = { "/save" },
            produces = { "application/json" },
            consumes = { "application/json" }
    )
    public ResponseEntity< TodoItem > createTodoItem( @RequestBody TodoItem todoItem ) {

        TodoItem savedItem = todoService.createItem( todoItem );

        return new ResponseEntity<>( savedItem, HttpStatus.CREATED );
    }

    @GetMapping(
            value = { "/all/{username}" },
            produces = { "application/json" }
    )
    public ResponseEntity< List< TodoItem > > getTodoList( @PathVariable String username ) {

        List< TodoItem > list = todoService.getTodoList( username );
        return new ResponseEntity<>( list, HttpStatus.OK );
    }

    @GetMapping(
            value = { "/{id}/{username}" },
            produces = { "application/json" }
    )
    public ResponseEntity< TodoItem > getTodoItem(
            @PathVariable String username, @PathVariable Long id ) {
        TodoItem item = todoService.getTodoItem( username, id );

        return ResponseEntity.status( HttpStatus.OK ).body( item );
    }

    @PutMapping(
            value = { "/update" },
            produces = { "application/json" },
            consumes = { "application/json" }
    )
    public ResponseEntity< TodoItem > updateTodoItem( @RequestBody TodoItem todoItem ) {
        TodoItem item = todoService.updateTodoItem( todoItem );
        return ResponseEntity.status( HttpStatus.OK ).body( item );
    }

    @DeleteMapping(
            value = { "/delete/{id}" }
    )
    public ResponseEntity< Void > deleteTodoItem( @PathVariable Long id ) {

        todoService.deleteTodoItem( id );
        return ResponseEntity.noContent().build();
    }
}
