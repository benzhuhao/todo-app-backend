package com.todo.app.persistence;

import com.todo.app.model.TodoItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoRepository extends JpaRepository< TodoItem, Long > {

    List< TodoItem > findTodoItemsByUsername( String username );

    TodoItem findTodoItemByUsernameAndId( String username, long id );

}
