package com.todo.app.persistence;

import com.todo.app.model.JwtUserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JwtUserRepo extends JpaRepository< JwtUserDetails, Long > {

    JwtUserDetails findJwtUserDetailsByUsername(String userName);
}
