package com.todo.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "Users")
public class JwtUserDetails implements UserDetails {

    private static final long serialVersionUID = 5155720064139820502L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private final Long id;

    @Column(nullable = false)
    @Size(min = 3, max = 10)
    @NotNull
    private String username;

    @Column(nullable = false)
    @Size(min = 3)
    @NotNull
    private String password;

    @Transient
    private final Collection< ? extends GrantedAuthority > authorities;

    public JwtUserDetails( Long id, String username, String password, String role ) {
        this.id = id;
        this.username = username;
        this.password = password;

        List< SimpleGrantedAuthority > authorities = new ArrayList<>();
        authorities.add( new SimpleGrantedAuthority( role ) );

        this.authorities = authorities;
    }

    @Override
    public Collection< ? extends GrantedAuthority > getAuthorities() {
        return authorities;
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
