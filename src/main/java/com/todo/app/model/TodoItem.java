package com.todo.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table( name = "Item" )
public class TodoItem implements Serializable {

    private static final long serialVersionUID = 8277837190593516198L;

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long id;

    private String username;
    private String description;
    private String lastUpdate;
    private boolean isDone = false;

    public TodoItem() {
    }

    public TodoItem(long id, String username, String description, String lastUpdate) {
        super();
        this.id = id;
        this.username = username;
        this.description = description;
        this.lastUpdate = lastUpdate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getlastUpdate() {
        return lastUpdate;
    }

    public void setlastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean isDone) {
        this.isDone = isDone;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TodoItem other = (TodoItem) obj;
        return id.equals( other.id );
    }
}
