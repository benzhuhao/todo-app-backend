package com.todo.app.service;

import com.todo.app.exception.TodoItemNotFountException;
import com.todo.app.exception.TodoItemNullException;
import com.todo.app.model.TodoItem;
import com.todo.app.persistence.JwtUserRepo;
import com.todo.app.persistence.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    @Autowired
    private TodoRepository todoRepository;

    @Autowired
    private JwtUserRepo jwtUserRepo;

    public TodoItem createItem( TodoItem todoItem ) {

        if( todoItem.getDescription() == null ) {
            throw new TodoItemNullException( "Todo item contents can not be null" );
        }

        return todoRepository.save( todoItem );
    }

    public TodoItem getTodoItem( String username, Long id ) {
        TodoItem item = todoRepository.findTodoItemByUsernameAndId( username, id );
        if( item == null ) {
            throw new TodoItemNotFountException( "Request item was not found" );
        }

        return item;
    }

    public List< TodoItem > getTodoList( String username ) {

        UserDetails user = jwtUserRepo.findJwtUserDetailsByUsername( username );

        if( user == null ) {
            throw new UsernameNotFoundException( String.format( "USER_NOT_FOUND '%s'.", username ) );
        }

        return todoRepository.findTodoItemsByUsername( username );
    }

    public TodoItem updateTodoItem( TodoItem todoItem ) {

        TodoItem todoItem1 = todoRepository
                .findTodoItemByUsernameAndId( todoItem.getUsername(), todoItem.getId() );

        if( todoItem1 == null ) {
            throw new TodoItemNotFountException( "Updated Todo Item fail, please make sure the todo item exist" );
        }

        return todoRepository.save( todoItem );
    }

    public void deleteTodoItem( Long id ) {

        todoRepository.deleteById( id );
    }
}
