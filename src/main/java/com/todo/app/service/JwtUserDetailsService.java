package com.todo.app.service;

import com.todo.app.persistence.JwtUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private JwtUserRepo jwtUserRepo;

    @Override
    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException {

        UserDetails user = jwtUserRepo.findJwtUserDetailsByUsername( username );

        if( user == null ) {
            throw new UsernameNotFoundException( String.format("USER_NOT_FOUND '%s'.", username) );
        }

        return user;
    }
}
